#!/bin/sh

set -e

export $(grep -v '^#' .env | xargs)

: "${DOCKER_REGISTRY:?DOCKER_REGISTRY not set or empty}"
# : "${AWS_ACCESS_KEY_ID:?AWS_ACCESS_KEY_ID not set or empty}"
# : "${AWS_SECRET_ACCESS_KEY:?AWS_SECRET_ACCESS_KEY not set or empty}"
# : "${AWS_DEFAULT_REGION:?AWS_DEFAULT_REGION not set or empty}"

aws ecr-public get-login-password --region $AWS_DEFAULT_REGION | docker login \
--username AWS --password-stdin $DOCKER_REGISTRY

docker build --no-cache \
--build-arg TERRAFORM_BINARY_VERSION=$TERRAFORM_BINARY_VERSION \
--build-arg TARGETARCH=$TARGETARCH \
--tag castellan/giterraws \
--file ./Dockerfile .

docker tag castellan/giterraws:latest $DOCKER_REGISTRY/castellan/giterraws:latest

docker push $DOCKER_REGISTRY/castellan/giterraws:latest
